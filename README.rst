########
aiia rst
########

.. begin-inclusion-intro-marker-do-not-remove

De AI Impact Assessment ('AIIA') in rst-format, om makkelijker te kunnen invoegen in documentatie. Dit komt overeen met versie 2 van de AI Impact Assesment.

.. end-inclusion-intro-marker-do-not-remove


.. begin-inclusion-usage-marker-do-not-remove

*******
Gebruik
*******

Deze vragenlijst is echt bedoeld als hulpmiddel: de AIIA zelf, te vinden op `Rijksoverheid Documenten <https://www.rijksoverheid.nl/documenten/rapporten/2022/11/30/ai-impact-assessment-ministerie-van-infrastructuur-en-waterstaat>`_, is leidend.
Vul de AIIA in met alle betrokkenen bij het project. In de via de link te vinden PDF staat ook relevante toelichting.

Invulvoorbeeld
--------------

Hieronder wordt een voorbeeld gegeven hoe het template technisch ingevuld dient te worden. Je dient er zelf zorg voor te dragen dat het antwoord dat je geeft uitgebreid genoeg is en voor iedereen duidelijk.

Het eerste code-block geeft een voorbeeld van hoe je de vragen terug vindt in het template:

.. code-block:: rst

  :blue:`i1. Geef een korte beschrijving van het beoogde AI-systeem (titel, algemene omschrijving, probleemstelling, en het domein)`

  .. Typ hier het antwoord


  :blue:`i2. Waarom is er voor de huidige techniek gekozen? (hierbij is het van belang dat alle afwegingen van robuustheid tot mensenrechten, impact op gebruiker en eindgebruiker, verantwoordingsplicht etc. zijn meegenomen in het antwoord)`

  .. Typ hier het antwoord

Dit dient technisch als volgt ingevuld te worden:

.. code-block:: rst

  :blue:`i1. Geef een korte beschrijving van het beoogde AI-systeem (titel, algemene omschrijving, probleemstelling, en het domein)`

  Dit is een korte beschrijving van het AI-systeem.


  :blue:`i2. Waarom is er voor de huidige techniek gekozen? (hierbij is het van belang dat alle afwegingen van robuustheid tot mensenrechten, impact op gebruiker en eindgebruiker, verantwoordingsplicht etc. zijn meegenomen in het antwoord)`

  Hier wordt uitgelegd waarom voor de huidige techniek gekozen is.


PDF generatie
-------------

LaTeX wordt gebruikt voor het genereren van een PDF van de AIIA. Hierin is het belangrijk om de juiste LaTeX instellingen aan Sphinx mee te geven. Dit zorgt ervoor dat de resulterende PDF duidelijk leesbaar is.

De LaTeX instellingen in Sphinx kunnen worden aangepast door onderstaande instellingen op te nemen in `doc/conf.py`:

.. code-block:: python

   latex_engine = "xelatex"
   latex_elements: Dict[str, str] = {
       "extraclassoptions": "openany,oneside",
       'preamble': r'''
   \definecolor{aiiablue}{HTML}{D2ECF8}
   \definecolor{aiiagreen}{HTML}{DDEEDB}
   \newsphinxbox[border-radius=0pt,border-width=0pt, background-TeXcolor={RGB}{210,236,248}]{\aiiablue}
   \newsphinxbox[border-radius=0pt,border-width=0pt, background-TeXcolor={RGB}{221,238,219}]{\aiiagreen}
   \newcommand{\DUroleblue}[1]{\aiiablue{\parbox{1\linewidth}{#1}}}
   \newcommand{\DUrolegreen}[1]{\aiiagreen{\parbox{1\linewidth}{#1}}}
   ''',
   }

.. end-inclusion-usage-marker-do-not-remove



************************
Uitgebreide documentatie
************************

De uitgebreide documentatie is `hier <https://rwsdatalab.gitlab.io/public/templates/aiia-rst>`_ te vinden.


.. begin-inclusion-license-marker-do-not-remove

********
Licentie
********

Copyright (c) 2024, Rijkswaterstaat


Licensed under the MIT License.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


.. end-inclusion-license-marker-do-not-remove
