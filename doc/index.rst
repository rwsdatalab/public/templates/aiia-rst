.. aiia rst documentation master file

AIIA rst documentatie
=====================

.. include:: ../README.rst
  :start-after: .. begin-inclusion-intro-marker-do-not-remove
  :end-before: .. end-inclusion-intro-marker-do-not-remove


.. toctree::
   :maxdepth: 1

   De AIIA vragenlijst <aiia.rst>
   Gebruik van de vragenlijst <usage.rst>
   Licentie <license.rst>
   Uitgaveopmerkingen <changelog.rst>

Hulp krijgen
------------

Problemen? Wij helpen graag!

- Rapporteer fouten op onze `issue tracker <https://gitlab.com/rwsdatalab/public/templates/aiia-rst/-/issues>`_.
- Zie dit document als `pdf <aiia-rst.pdf>`_.
