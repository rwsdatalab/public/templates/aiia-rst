.. raw:: html

    <style> .blue{width:100%; border:none;background-color: #D2ECF8;padding:4px 0px} </style>
    <style> .green{width:100%; border:none;background-color: #DDEEDB;padding:4px 0px} </style>

.. role:: blue
.. role:: green

###################
De AIIA vragenlijst
###################

Dit zijn de vragen uit de AI Impact Assessment. In `het document zelf <https://www.rijksoverheid.nl/documenten/rapporten/2022/11/30/AI-impact-assessment-ministerie-van-infrastructuur-en-waterstaat>`_ staat essentiële toelichting.

Daarnaast kan het zijn dat de AIIA gewijzigd is sinds deze formatting. Houd dus altijd de officiële AIIA aan bij het bespreken van de vragen.

*****************
Inleidende vragen
*****************

Doel van het systeem
====================

:blue:`i1. Geef een korte beschrijving van het beoogde AI-systeem (titel, algemene omschrijving, probleemstelling, en het domein)`

.. Typ hier het antwoord


:blue:`i2. Waarom is er voor de huidige techniek gekozen? (hierbij is het van belang dat alle afwegingen van robuustheid tot mensenrechten, impact op gebruiker en eindgebruiker, verantwoordingsplicht etc. zijn meegenomen in het antwoord)`

.. Typ hier het antwoord

:blue:`i3. Wat is het doel en beoogde resultaat van het AI-systeem?`

.. Typ hier het antwoord

:blue:`i4. Welk doel wordt er aan het AI-systeem gekoppeld volgens het rapport Aandacht voor Algoritmes van de Algemene Rekenkamer? doel 1, doel 2 of doel 3?`

.. Typ hier het antwoord


Rol binnen de organisatie
=========================

:blue:`i5. Waar in de organisatie is beoogd het AI-systeem te gebruiken en welke beoogde impact is er voor de organisatie?`

.. Typ hier het antwoord

:blue:`i6. Beschrijf de rolverdeling binnen het opzetten van het AI-systeem (zoals de ontwikkelaar, opdrachtgever, projectleider, beheerorganisaties en eindverantwoordelijke).`

.. Typ hier het antwoord

:blue:`i7. Wie is de gebruiker van het AI-systeem, wie zijn de eindgebruikers die met het systeem werken en welke betrokkenen ondervinden impact van het AI-systeem?`

.. Typ hier het antwoord


*******************************
Fundamentele rechten & fairness
*******************************

Grondrechten
============

:blue:`f1. Wat is de mogelijke impact op de grondrechten van burgers door het gebruik van het AI-systeem?`

.. Typ hier het antwoord

:blue:`f2. Is het proportioneel en subsidiair om dit systeem in te zetten om de gestelde doelen te realiseren? Oftewel: is de impact in verhouding met de beoogde doelen en zijn er geen andere minder ingrijpende manieren om deze doelen te behalen?`

.. Typ hier het antwoord

:blue:`f3. Wat is de wettelijke grondslag van de inzet van het AI-systeem en van de beoogde besluiten die genomen worden op basis van het AI-systeem?`

.. Typ hier het antwoord

:green:`fo1. Welke grondrechtelijke bepalingen zijn mogelijk van toepassing?`

.. Typ hier het antwoord

:green:`fo2. Op welk van deze grondrechtelijke bepalingen kan mogelijk een inbreuk worden gemaakt bij verkeerde uitvoering van het AI-systeem Welke acties worden genomen om dit te voorkomen?`

.. Typ hier het antwoord


Bias
====

:blue:`f4. Hoe wordt rekening gehouden met mogelijk onwenselijke bias in de input, bias in het model en bias in de output van het AI-systeem?`

.. Typ hier het antwoord


Bias in de input(data)
""""""""""""""""""""""

:green:`fo3. Is de input(data) data representatief voor het onderwerp waarover een beslissing moet worden genomen?`

.. Typ hier het antwoord

:green:`fo4. Worden indien nodig subpopulaties beschermd bij het trekken van steekproeven?`

.. Typ hier het antwoord

:green:`fo5. Is de keuze voor de inputvariabelen onderbouwd en afgestemd met de betrokkenen?`

.. Typ hier het antwoord


Bias in het model
""""""""""""""""""""""

:green:`fo6. Op welke manier wordt er rekening gehouden met het feit dat er geen onterechte of onrechtvaardige bias in een AI-systeem wordt gecreëerd of versterkt?`

.. Typ hier het antwoord

:green:`fo7. Is het AI-systeem te gebruiken door de beoogde eindgebruikers (dus ongeacht diens kenmerken zoals leeftijd, geslacht of capaciteit)?`

.. Typ hier het antwoord


Bias in de output(data)
"""""""""""""""""""""""

:green:`fo8. Zijn er stop-, toezicht- of controle- mechanisme ingesteld om te voorkomen dat groepen in de maatschappij disproportioneel getroffen kunnen worden door de negatieve implicaties van het AI-systeem?`

.. Typ hier het antwoord


Stakeholderparticipatie
=======================

:blue:`f5. Zijn alle stakeholders in kaart gebracht middels een stakeholderanalyse en is met hen het gesprek aangegaan?`

.. Typ hier het antwoord

:green:`fo11. Welke feedback is er verzameld van teams of groepen die verschillende achtergronden en ervaringen representeren? En wat is hier vervolgens mee gedaan?`

.. Typ hier het antwoord

:green:`fo12. Hoe wordt de invoering van het AI-systeem geïntroduceerd richting collega’s van IenW?`

.. Typ hier het antwoord

:green:`fo13. Hoe wordt de invoering van het AI-systeem geïntroduceerd richting de samenleving?`

.. Typ hier het antwoord


**********************
Technische robuustheid
**********************

Accuraatheid
============

:blue:`t1. Hoe wordt de doorlopende accuraatheid van het systeem gemeten en gewaarborgd?`

.. Typ hier het antwoord

:green:`to1. Wat zijn de opgezette acceptatiecriteria om de kwaliteit van de input(data) en output(data) van het model aan te toetsen?`

.. Typ hier het antwoord

:green:`to2. Passen de acceptatiecriteria bij de data en het doel van het AI-systeem?`

.. Typ hier het antwoord

:green:`to3. Welke evaluatie meetsystemen (performance metrics) ga je gebruiken om de acceptatiecriteria te waarborgen en waarom?`

.. Typ hier het antwoord

:green:`to4. Hoe wordt de output(data) (periodiek) steekproefsgewijs en doorlopend getest op juistheid?`

.. Typ hier het antwoord

:green:`to5. Hoe worden afwijkingen in de output(data) ten opzichte van acceptatiecriteria tijdig geanalyseerd en gecorrigeerd?`

.. Typ hier het antwoord

:green:`to6. Wat zijn de resultaten als er alternatieve modellen zouden worden ingezet?`

.. Typ hier het antwoord


Betrouwbaarheid
===============

:blue:`t2. Is het AI-systeem betrouwbaar?`

.. Typ hier het antwoord

:green:`to7. Wat zijn de belangrijkste factoren die de prestaties van het AI-systeem beïnvloeden?`

.. Typ hier het antwoord

:green:`to8. Wordt een deel van de (sub)dataset uitgesloten voor het leren van het model en alleen gebruikt voor het bepalen van de betrouwbaarheid of wordt de betrouwbaarheid van het model berekend met behulp van cross-validatie?`

.. Typ hier het antwoord

:green:`to9. Hoe is de (hyper)parameter-tuning onderbouwd en getoetst?`

.. Typ hier het antwoord


Technische implementatie
========================

:blue:`t3. Hoe is het AI systeem technisch geïmplementeerd?`

.. Typ hier het antwoord

:green:`to10. Is er nagedacht hoe het AI-systeem past in de al bestaande technische- en systeeminfrastructuur en zijn hier passende maatregelen voor genomen om deze uit te rollen (indien van toepassing)?`

.. Typ hier het antwoord

:green:`to11. Hoe ziet de systeemarchitectuur eruit (hoe verhouden de softwarecomponenten zicht tot elkaar)?`

.. Typ hier het antwoord

:green:`to12. Zijn eventuele specifieke hardware- en software-eisen gedocumenteerd?`

.. Typ hier het antwoord


Reproduceerbaarheid
===================

:blue:`t4. Is het AI-systeem reproduceerbaar? Is er een proces ingesteld om dit te meten?`

.. Typ hier het antwoord

:green:`to13. Kan je een verkregen output(data) nu of in de toekomst reconstrueren (dus bijvoorbeeld zijn oude versies van het model, datasets en omstandigheden opgeslagen middels versiebeheer)?`

.. Typ hier het antwoord

:green:`to14. Is het mogelijk om gegeven de parameters en een vaste seed het model te reconstrueren?`

.. Typ hier het antwoord

:green:`to15. Is het AI-systeem aan de hand van documentatie op hoofdlijnen te reproduceren?`

.. Typ hier het antwoord

:green:`to16. Hoe worden de wijzigingen tijdens de levensduur van het systeem gedocumenteerd?`

.. Typ hier het antwoord


Uitlegbaarheid
==============

:blue:`t5. Is het AI-systeem voldoende uitlegbaar en te interpreteren voor de ontwikkelaars?`

.. Typ hier het antwoord

:green:`to17. Hoe heb je bij het ontwikkelen van het AI-systeem gekeken naar de uitlegbaarheid van het model?`

.. Typ hier het antwoord

:green:`to18. In hoeverre is het mogelijk om een verklaring te geven aan een externe AI-expert hoe het AI-systeem op een bepaalde manier werkt (zie ook ‘Uitlegbaarheid’)?`

.. Typ hier het antwoord

:green:`to19. Is de benodigde deskundigheid voor het beheer van AI-systeem gedocumenteerd?`

.. Typ hier het antwoord


***************
Data governance
***************

Kwaliteit en integriteit van data
=================================

:blue:`d1. Hoe wordt de kwaliteit van de data gewaarborgd?`

.. Typ hier het antwoord


Overkoepelend
"""""""""""""

:green:`do1. Is de gebruikte data noodzakelijk voor het AI-systeem?`

.. Typ hier het antwoord

:green:`do2. Hoe voorkom je onbedoelde verdubbelingen van data?`

.. Typ hier het antwoord

:green:`do3. Is het mogelijk om de trainings- en testgegevens te actualiseren als de situatie daar om vraagt? Wanneer besluit je het AI-systeem te her-trainen, tijdelijk stop te zetten, of door te ontwikkelen?`

.. Typ hier het antwoord


Input(data)
"""""""""""

:green:`do4. Voldoet de data aan de aannames van het model?`

.. Typ hier het antwoord

:green:`do5. Op welke manier is de input(data) die wordt gebruikt in het AI-systeem verzameld en samengevoegd?`

.. Typ hier het antwoord

:green:`do6. Hoe wordt de data gelabeld?`

.. Typ hier het antwoord

:green:`do7. Welke factoren hebben invloed op de kwaliteit van de input(data)? En wat kan je daaraan doen?`

.. Typ hier het antwoord

:green:`do8. Is de input(data) getoetst op veranderingen die zich voordoen tijdens trainen, testen en evalueren? Ook door de tijd heen tijdens het gebruik van het algoritme?`

.. Typ hier het antwoord


Output(data)
""""""""""""
:green:`do9. Indien output(data) wordt gebruikt als nieuwe input, hoe wordt de output(data) opgeslagen (denk aan een feedbackloop)?`

.. Typ hier het antwoord

:green:`do10. Hoe zorg je ervoor dat de output(data) tijdig beschikbaar is?`

.. Typ hier het antwoord


Privacy en gegevensbescherming
==============================

:blue:`d2. Hoe wordt er omgegaan met persoonsgegevens of vertrouwelijke gegevens? (Denk aan de DPIA)`

.. Typ hier het antwoord


Met betrekking tot persoonsgegevens
"""""""""""""""""""""""""""""""""""

:green:`do11. Werkt het AI-systeem met persoonsgegevens (is de AVG van toepassing)? Zo ja, vul de volgende vragen ook in. Zo nee, ga verder bij ‘met betrekking tot vertrouwelijke gegevens’.`

.. Typ hier het antwoord

:green:`do12. Is de output van het AI-systeem tot personen te herleiden (is de AVG van toepassing)? Zo ja, vul dan de volgende vragen ook in.`

.. Typ hier het antwoord

:green:`do13. Zijn er verregaande beschermingsmaatregelen genomen om de persoonsgegevens te beveiligen?`

.. Typ hier het antwoord

:green:`do14. Zijn functionarissen betrokken, zoals de functionaris gegevensbescherming, privacy adviseur, informatiebeveiliger, Chief Information Officer, etc.?`

.. Typ hier het antwoord

:green:`do15. Hoe vaak wordt de kwaliteit en de noodzakelijkheid van de verwerking van persoonsgegevens geëvalueerd?`

.. Typ hier het antwoord

:green:`do16. Is er aandacht besteed aan rechten van derden met betrekking tot verspreiding van informatie over het AI-systeem?`

.. Typ hier het antwoord


Met betrekking tot vertrouwelijke gegevens (niet zijnde persoonsgegevens)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

:green:`do17. Worden vertrouwelijke gegevens gebruikt of opgeslagen?`

.. Typ hier het antwoord

:green:`do18. Hoe wordt de veiligheid van deze informatie gewaarborgd?`

.. Typ hier het antwoord


************
Risicobeheer
************

Risicobeheersing
================

:blue:`r1. Hoe is het AI-systeem getest op de passende risicobeheersmaatregelen?`

.. Typ hier het antwoord

:green:`ro1. Hoe is de toegang tot het AI-systeem en diens componenten ingericht? (Denk aan de Generieke IT-beheersmaatregelen)`

.. Typ hier het antwoord

:green:`ro2. Hoe is het AI-systeem getest op het beoogde doel voordat het in gebruik wordt genomen?`

.. Typ hier het antwoord

:green:`ro3. Is het waarschijnlijk dat kwetsbare groepen (zoals kinderen) toegang zullen hebben tot het AI-systeem? In dat geval moeten de risicobeheersmaatregelen extra worden aangescherpt.`

.. Typ hier het antwoord

:green:`ro4. Zijn er buiten de standaard beveiligingsmaatregelen van IenW extra maatregelen genomen om het AI-systeem te beveiligen?`

.. Typ hier het antwoord

:green:`ro5. Hoe wordt het alternatieve plan als er problemen met het AI-systeem zijn in werking gezet?`

.. Typ hier het antwoord

:green:`ro6. Is de correctheid van de implementatie aangetoond? Denk hierbij bijvoorbeeld aan unitintegratie- en end-to-end tests, indien van toepassing`.

.. Typ hier het antwoord

:green:`ro7. Hoe kan het AI-systeem interageren met andere hardware of software (indien van toepassing)?`
.. Typ hier het antwoord


Alternatieve werkwijze
======================

:blue:`r2. Wat is het plan als er problemen met de werking van het AI-systeem zijn?`

.. Typ hier het antwoord

:green:`ro8. Wat is de impact als het AI-systeem uitvalt?`

.. Typ hier het antwoord

:green:`ro9. Zie hierboven het voorbeeld over de rekenmachine. Wat is een equivalent effect wat kan optreden als het AI-systeem in gebruik wordt genomen, en is dit wenselijk?`

.. Typ hier het antwoord

:green:`ro10. Is het AI-systeem bestand tegen fouten of onregelmatigheden van interactie met natuurlijke personen of andere systemen?`

.. Typ hier het antwoord


Hackaanvallen en corruptie
==========================

:blue:`r3. Op welke manier worden informatiebeveiligingsrisico’s inzichtelijk gemaakt, teruggebracht naar een acceptabel niveau en (technisch) getest?`

.. Typ hier het antwoord

:green:`ro11. Hoe wordt er voorkomen dat ongeautoriseerde derden gebruik kunnen maken van kwetsbaarheden van het AI-systeem?`

.. Typ hier het antwoord

:green:`ro12. Wat is de impact als derden ongewenst toegang hebben tot de broncode, data of uitkomsten van het AI-systeem?`

.. Typ hier het antwoord

:green:`ro13. Kunnen mensen misbruik maken van het feit dat er een AI-systeem wordt ingezet in plaats van een menselijke beslissing?`

.. Typ hier het antwoord

:green:`ro14. Hoe wordt er geregistreerd wie er gebruik maakt van het AI-systeem en hoe lang?`

.. Typ hier het antwoord


*********************
Verantwoordingsplicht
*********************

Communicatie
============

:blue:`v1. Ben je transparant richting betrokkenen en eindgebruikers over de beperkingen en werking van het AI-systeem? En blijven deze voldoende onder de aandacht zolang ze bestaan?`

.. Typ hier het antwoord

:blue:`v2. Worden er mechanismes ingesteld waarin eindgebruikers opmerkingen over het systeem (data, techniek, doelgroep, etc.) kunnen maken? En hoe of wanneer worden deze meldingen gewaarborgd (geanalyseerd en gevolgd)?`

.. Typ hier het antwoord


Communicatie met het AI-systeem
"""""""""""""""""""""""""""""""

:green:`vo1. Wordt er aan de eindgebruiker en betrokkenen van het AI-systeem gecommuniceerd dat de resultaten gegenereerd worden door een AI-systeem en wat dat voor hen betekent?`

.. Typ hier het antwoord

:green:`vo2. Zijn er eindgebruiksinstructies opgesteld? Deze moeten minstens het volgende bevatten:`
        - :green:`De naam en contactgegevens van de aanbieder;`
        - :green:`Kenmerken, capaciteiten en beperkingen;`
        - :green:`Mogelijke toekomstige wijzigingen;`
        - :green:`Menselijk toezicht;`
        - :green:`Verwachte levensduur.`

.. Typ hier het antwoord

:green:`vo3. Wat zijn de potentiële (psychologische) bijwerkingen zoals het risico op verwarring, voorkeur of cognitieve vermoeidheid van de eindgebruiker bij het gebruik maken van het AI-systeem?`

.. Typ hier het antwoord


Communicatie over de uitkomsten AI-systeem
""""""""""""""""""""""""""""""""""""""""""

:green:`vo4. In hoeverre is het mogelijk om een verklaring te geven aan een betrokkene waarom het AI-systeem op een bepaalde manier werkt?`

.. Typ hier het antwoord

:green:`vo5. Is het systeem voldoende transparant om eindgebruikers in staat te stellen de output(data) van het systeem te interpreteren en op passende wijze te gebruiken?`

.. Typ hier het antwoord

:green:`vo6. Is er iets ingericht om eindgebruikers eventuele bijscholing te verlenen?`

.. Typ hier het antwoord


Communicatie naar aanleiding van het AI-systeem
"""""""""""""""""""""""""""""""""""""""""""""""

:green:`vo7. Hoe wordt ervoor gezorgd dat commentaar van betrokkenen en eindgebruikers intern goed wordt behandeld?`

.. Typ hier het antwoord

:green:`vo8. Als een betrokkene bezwaar wil aantekenen, of een klacht wil indienen tegen een besluit van het AI-systeem, is het dan duidelijk welke stappen hij/zij kan nemen? Hetzelfde geldt voor beroep instellen.`

.. Typ hier het antwoord


Controleerbaarheid
==================

:blue:`v3. Hoe wordt het AI-systeem gecontroleerd?`

.. Typ hier het antwoord

:blue:`v4. Hoe is menselijke controle en toezicht gewaarborgd?`

.. Typ hier het antwoord

:green:`vo9. Hoe wordt rekening gehouden met het ingaan van aangekondigde nieuwe wet- en regelgeving tijdens de levensduur van dit AI-systeem?`

.. Typ hier het antwoord

:green:`vo10. Hoe wordt ervoor gezorgd dat het AI-systeem onafhankelijk kan worden gecontroleerd?`

.. Typ hier het antwoord

:green:`vo11. Hoe wordt de correctheid van de input(data) gecontroleerd en geïnterpreteerd?`

.. Typ hier het antwoord

:green:`vo12. Hoe wordt de correctheid van het model gecontroleerd en geïnterpreteerd?`

.. Typ hier het antwoord

:green:`vo13. Hoe wordt de correctheid van de output(data) gecontroleerd en geïnterpreteerd?`

.. Typ hier het antwoord


Archivering
===========

Input(data)
"""""""""""

:green:`vo14. Hoe wordt de input(data) opgeslagen?`

.. Typ hier het antwoord

:green:`vo15. Wat is de bewaartermijn van de input(data)?`

.. Typ hier het antwoord


Model
"""""

:green:`vo16. Hoe wordt het model opgeslagen?`

.. Typ hier het antwoord


Output(data)
""""""""""""

:green:`vo17. Kunnen de gebruikers de output(data) op de juiste manier interpreteren?`

.. Typ hier het antwoord

:green:`vo18. Wat is de bewaartermijn van de output(data)?`

.. Typ hier het antwoord


Klimaatadaptatie
================

:green:`vo19. Is er impact op het milieu door het invoeren van het AI-systeem (ontwikkeling, installatie en gebruik), en hoe wordt dit gemeten?`

.. Typ hier het antwoord

:green:`vo20. Hoe wordt de impact van het AI-systeem afgewogen tegen de milieukosten van het laten draaien van het AI-systeem?`

.. Typ hier het antwoord

:green:`vo21. Wat voor maatregelen zijn er genomen om de milieu-impact van het AI-systeem te minimaliseren?`

.. Typ hier het antwoord
