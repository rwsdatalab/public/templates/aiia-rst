******************
Uitgaveopmerkingen
******************

Alle noemenswaardige veranderingen worden gedocumenteerd in dit bestand.
Binnen dit project volgen we `Semantic Versioning for Documents <http://semverdoc.org/>`_.

[0.1.1]
=======

Aangepast
"""""""""

* Gebruik kleuraanduiding in gegenereerde PDF
* Correctie typos


[0.1.0]
=======

Toegevoegd
""""""""""

* Eerste versie van de AIIA vragenlijst
